/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author smabe
 */
public class S5_2_Planned_Task_Observations_TestSuite
{
    static TestMarshall instance;

    public S5_2_Planned_Task_Observations_TestSuite()
    {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.coreBeta;
    }

    
    //full run
      @Test
    public void S5_2_Planned_Task_Observations_QA01S5_2() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\S5_2_Planned_Task_Observations_QA01S5_2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    @Test
    public void FR5_View_Reports_Main_Scenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Planned Task Observations v5.2\\FR5 View Reports Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    //FR1 - Capture Planned Task Observations - Main Scenario
    @Test
    public void FR1_Capture_Planned_Task_Observations_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Planned Task Observations v5.2\\FR1-Capture Planned Task Observations - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR1_Capture_Planned_Task_Observations_AlternateScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Planned Task Observations v5.2\\FR1-Capture Planned Task Observations - Alternate Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR1_Capture_Planned_Task_Observations_OptionalScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Planned Task Observations v5.2\\FR1-Capture Planned Task Observations - Optional Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    //FR2 - Capture Findings Recommendations Actions - Main Scenario
    @Test
    public void FR2_Capture_Findings_Recommendations_Actions_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Planned Task Observations v5.2\\FR2-Capture Findings, Recommendations, Actions - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    //FR3 - Complete Declaration To Close PTO - MainScenario
    @Test
    public void FR3_Complete_Declaration_To_Close_PTO_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Planned Task Observations v5.2\\FR3-Complete Declaration to Close PTO - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

}
