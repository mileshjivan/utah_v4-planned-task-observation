/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author Ethiene
 */
public class WinAppSuite {

    static TestMarshall instance;
    public static Enums.DeviceConfig test;

    public WinAppSuite() {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentDesktopApplication = Enums.DesktopApplication.VLC;

    }
    @Test
    public void winAppTestSuite() throws FileNotFoundException {

        Narrator.logDebug("Win - Test Pack");
        instance = new TestMarshall("TestPacks\\WinApp.xlsx");
        instance.runKeywordDrivenTests();
    }
}
