/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Planned_Task_Observations_V5_2_PageObjetcs;



/**
 *
 * @author SKhumalo
 */
public class MailSlurper_PageObjects {
    public static String mailSlurper(){
        return "http://qa01.isometrix.net:8090/";
    }
    
      public static String iframeXpath()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String Username() {
        return "//input[@id='userName']";
    }

    public static String Password() {
        return "//input[@id='password']";
    }
    
    public static String PermitToWork_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_41B8AE76-F4F5-4CA6-97C2-8DCB7A749951']";
    }

    public static String LoginBtn() {
        return "//button[@id='btnSubmit']";
    }

    public static String recordLink()
    {
        return "//div[@class='row'][3]//tr[1]//td//a[contains(text(),'#')]";
    }

    public static String refreshBtn()
    {
        return "//button[@id='btnRefresh']";
    }

    public static String linkBackToRecord()
    {
        return "//a[text()='Link back to record']";
    }

    public static String searchBtn()
    {
        return "//button[@id='btnSearch']";
    }

    public static String recordNo()
    {
        return "//input[@id='txtMessage']";
    }

    public static String search_Btn()
    {
        return "//button[@id='btnExecuteSearch']";
    }

}
